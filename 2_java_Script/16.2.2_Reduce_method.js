//? Description:
//?  --> The reduce() method executes a reducer function for array element.
//?  --> The reduce() method returns a single value: the function's accumulated result.
//?  --> The reduce() method does not execute the function for empty array elements.
//?  --> The reduce() method does not change the original array.

//?Note
//?    -->  At the first callback, there is no return value from the previous callback.
//?    -->  Normally, array element 0 is used as initial value, and the iteration starts from array element 1.
//?    -->  If an initial value is supplied, this is used, and the iteration starts from array element 0.

//Syntax:
//     array.reduce(function(total, currentValue, currentIndex, arr), initialValue)

let array = [
  5, 12, 8, 20, 3, 17, 10, 25, 1, 15, 9, 30, 7, 22, 18, 6, 11, 4, 28, 14,
];

//!   find max from array
let r1 = array.reduce((acc, cur) => {
  acc = acc + cur;
  return acc;
}, 0);
console.log(r1);

//! finding max value from array

const r2 = array.reduce((acc, cur) => {
  if (cur > acc) {
    acc = cur;
  }
  return acc;
}, 0);
console.log(r2);

let people = [
  { firstName: "John", lastName: "Doe", age: 30 },
  { firstName: "Jane", lastName: "Smith", age: 25 },
  { firstName: "Michael", lastName: "Johnson", age: 40 },
  { firstName: "Emily", lastName: "Williams", age: 35 },
  { firstName: "David", lastName: "Brown", age: 25 },
];

// list of full name
// ["John Doe","Jane Smith","Michael Johnson","Emily Williams","David Brown"]

let r4 = people.map((people) => {
  return people.firstName + " " + people.lastName;
});
console.log(r4);

//{ 30 : 1, 25 : 2, 40 : 1 35 : 1}

let r5 = people.reduce((acc, cur) => {
  if (acc[cur.age]) {
    acc[cur.age] = ++acc[cur.age];
  } else {
    acc[cur.age] = 1;
  }
  return acc; // Important: Always return the accumulator in reduce
}, {});

console.log(r5);

//!  frist name whose age  greater than 30
let r6 = people.filter((str) => str.age > 30).map(str=>str.firstName);
console.log(r6);


//!   use reduce and done  above

let r7=people.reduce((acc,cur)=>{
   if(cur.age<30)
   {
    acc.push(cur.firstName)
   }
   return acc
},[])
console.log(r7);
