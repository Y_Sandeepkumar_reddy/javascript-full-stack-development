//? The findIndex() method executes a function for each array element.

//? The findIndex() method returns the index (position) of the first element that passes a test.

//? The findIndex() method returns -1 if no match is found.

//? The findIndex() method does not execute the function for empty array elements.

//? The findIndex() method does not change the original array.


//     syntax:

//        array.findIndex(function(currentValue, index, arr), thisValue)




/*
indexOf()	      - The index of the first element with a specified value
lastIndexOf()     - The index of the last element with a specified value
find()	          - The value of the first element that passes a test
findIndex()	      - The index of the first element that passes a test
findLast()	      - The value of the last element that passes a test
findLastIndex()	  - The index of the last element that passes a test
*/


let a=[1,3,5,7,9,2,4,6,8,9,10]

let b=a.findIndex(num)

function num(number) {
    return number===9
}
num(a)
console.log(b);




let books = [
    { title: 'Book 1', category: 'Fiction' },
    { title: 'Book 2', category: 'Non-Fiction' },
    { title: 'Book 3', category: 'Fiction' }
  ];
  
  // Custom function to find the index of the first 'Non-Fiction'  book
  function findFictionIndex(book) {
    return book.category === 'Non-Fiction' ;
  }
  
  let index = books.findIndex(findFictionIndex);
  
  console.log(index); // Output: 0 (index of the first 'Non-Fiction'  book)
  