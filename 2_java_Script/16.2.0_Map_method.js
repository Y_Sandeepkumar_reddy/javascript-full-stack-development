//?  map() creates a new array from calling a function for every array element.

//?  map() does not execute the function for empty elements.

//?  map() does not change the original array.

//! program to doublem the array
let arr1 = [2, 6, 9, 12, 18, 19];

const r1 = arr1.map(double);

function double(numb) {
  return numb * 2;
}
console.log(r1);

// !    program to  thrible the array
const r2 = arr1.map((numb) => {
  return numb * 3;
});
console.log(r2);

//! binary

let r3 = arr1.map((binary) => {
  return binary.toString(2);
});
console.log(r3);

let people = [
  { firstName: "John", lastName: "Doe", age: 30 },
  { firstName: "Jane", lastName: "Smith", age: 25 },
  { firstName: "Michael", lastName: "Johnson", age: 40 },
  { firstName: "Emily", lastName: "Williams", age: 35 },
  { firstName: "David", lastName: "Brown", age: 25 },
];

// list of full name
// ["John Doe","Jane Smith","Michael Johnson","Emily Williams","David Brown"]

let r4 = people.map((people) => {
  return people.firstName + " " + people.lastName;
});
console.log(r4);

//{ 30 : 1, 25 : 2, 40 : 1 35 : 1}


