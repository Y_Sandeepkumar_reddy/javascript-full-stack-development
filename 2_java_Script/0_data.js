let colors = [
    'Red', 'Green', 'Blue', 'Yellow', 'Orange',
    'Purple', 'Pink', 'Brown', 'Black', 'White',
    'Gray', 'Cyan', 'Magenta', 'Lime', 'Teal',
    'Navy', 'Maroon', 'Olive', 'Silver', 'Gold','Black'
];
let fruits = [
    'Apple', 'Banana', 'Orange', 'Grapes', 'Watermelon',
    'Pineapple', 'Mango', 'Strawberry', 'Cherry', 'Kiwi',
    'Peach', 'Plum', 'Pear', 'Lemon', 'Coconut',
    'Apricot', 'Blueberry', 'Raspberry', 'Cantaloupe', 'Pomegranate'
];
let  numbers=[1,2,3,[4,5,[6,7,8,[9,10,11,12]]]];



let data={
    company: "Acme Corporation",
    employees: [
      {
        "id": 1,
        "name": "John Doe",
        "department": "Engineering",
        "skills": ["JavaScript", "Python", "Java"],
        "projects": [
          {
            "name": "Project A",
            "description": "Developing a new web application",
            "status": "In Progress"
          },
          {
            "name": "Project B",
            "description": "Implementing machine learning algorithms",
            "status": "Completed"
          }
        ]
      },
      {
        "id": 2,
        "name": "Jane Smith",
        "department": "Marketing",
        "skills": ["Marketing Strategy", "Social Media", "Content Creation"],
        "projects": [
          {
            "name": "Campaign X",
            "description": "Launching a new product campaign",
            "status": "Planned"
          },
          {
            "name": "Campaign Y",
            "description": "Optimizing ad spend for better ROI",
            "status": "Active"
          }
        ]
      }
    ],
    products: [
      {
        "id": "P001",
        "name": "Product A",
        "category": "Electronics",
        "price": 499.99,
        "reviews": [
          {
            "user": "Alice",
            "rating": 4,
            "comment": "Great product, highly recommended."
          },
          {
            "user": "Bob",
            "rating": 5,
            "comment": "Excellent quality and performance."
          }
        ]
      },
      {
        "id": "P002",
        "name": "Product B",
        "category": "Clothing",
        "price": 59.99,
        "reviews": [
          {
            "user": "Carol",
            "rating": 3,
            "comment": "Decent product for the price."
          },
          {
            "user": "David",
            "rating": 4,
            "comment": "Comfortable and stylish."
          }
        ]
      }
    ]
  }

module.exports = { colors, fruits, numbers, data };
