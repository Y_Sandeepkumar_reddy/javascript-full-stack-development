// level 2 methods


/*  
1. forEach         5 find                  9. reduce
2. map             6.findIndex             10. for of 
3. filter          7. every                11. for in
4. sort            8. some                 

*/

let colors = [
    'Red', 'Green', 'Blue', 'Yellow', 'Orange',
    'Purple', 'Pink', 'Brown', 'Black', 'White',
    'Gray', 'Cyan', 'Magenta', 'Lime', 'Teal',
    'Navy', 'Maroon', 'Olive', 'Silver', 'Gold','Black'
];
let fruits = [
    'Apple', 'Banana', 'Orange', 'Grapes', 'Watermelon',
    'Pineapple', 'Mango', 'Strawberry', 'Cherry', 'Kiwi',
    'Peach', 'Plum', 'Pear', 'Lemon', 'Coconut',
    'Apricot', 'Blueberry', 'Raspberry', 'Cantaloupe', 'Pomegranate'
];
let  numbers=[1,2,3,[4,5,[6,7,8,[9,10,11,12]]]];

// for of
//  for of is used to iterate each element in an array or  string 
let a=(n)=>{
    allcolors=[]
    for(Element of n)
    {
      allcolors+=Element+" "
    }
    return allcolors.trim()
}
console.log(a(colors));
console.log(a(fruits));
console.log(a(numbers));


console.log("---------------------------------------------------------------");


//  for in

// The for...in statement in JavaScript is used to iterate over the enumerable properties of an object
const colorsObject = {
    r: 'Red',
    g: 'Green',
    b: 'Blue',
    y: 'Yellow'
};

let a2 = (n) => {
    let obj = {};
    for (const key in n) {
        if (n.hasOwnProperty(key)) {
            obj[key] = n[key];
            console.log(`${key}: ${n[key]}`);
        }
    }
    return obj; 
};

console.log(a2(colorsObject));


const bigObject = {
    name: 'John Doe',
    age: 30,
    address: {
        street: '123 Main St',
        city: 'New York',
        zip: '10001'
    },
    contact: {
        email: 'john.doe@example.com',
        phone: '+1 123-456-7890'
    }
   
};


let a1=(object)=>{
    for (const key in object) {
        if(object.hasOwnProperty(key))
        {
           if(object.address.city==='New York')
            {
                object.address.user="enemy"
            }
        }
    }
    return object
}
console.log((a1(bigObject)));


console.log("--------------------------------------------------------------------------");






