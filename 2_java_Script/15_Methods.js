// Function vs Method 


//Function

/*
--> A function is a block of code , That performs a perticular task, And Java script Function is executed when someone invoked it.
-->functions are stand alone and  called any where in the our code
-->there are not tied to any specific class or object

*/

// Methods

/*
-->A Method in a javascript is  just like function and associated with object
-->Methods are created inside the object to perform certain operations related to the object.
*/


 // 1. 

 function add(a, b) {
    return a + b;
}

let operations = {
    sum: add
};

// Example usage:
console.log(operations.sum(3, 5)); // Output: 8



// 2.


let message="hi"

console.log(message.toUpperCase());    // HI



let num=3.1415
console.log(num.toFixed(3))    // 3.142


//  in the code is primitive value how it work as object  ?
 
// --> message is string value but , as soon as you add  (.) operator after there is a wrapper that get created . 
// --> wrapper means  there is a huge bag  that contains a lot of methods  like  toUppercase , toLowercase, toFix, etc.

/*
1.charAt        :         6.trimStart     :      11.lastIndexOf  :      16. slice       :
2.toUppercase   :         7.cancat        :      12.padEnd       :      17. split       :
3.toLowercase   :         8.endWith       :      13.padStart     :      18. subString   :
4. trim         :         9.includes      :      14. repeat      :
5.trimEnd       :         10.indexOf      :      15. replay      :
 
*/




// Write a function that takes a string and an index as arguments and returns the character at that index using the charAt method.
console.log(" ");
console.log("1:");
 function s1(string,index) {
    console.log(string.charAt(index))
 }
 s1("Sandeep",3)

//  Write a function that takes a string as input and returns the string converted to uppercase using the toUpperCase method.
console.log(" ");
console.log("2:");
function uppercase(str) {
    return  a= str.toUpperCase();
}
uppercase("Hi Sandeep")
console.log(a);

// Write a function that takes a string as input and returns the string converted to lowercase using the toLowerCase method.

console.log(" ");
console.log("3:");
function uppercase(str) {
     a= str.toUpperCase();
     b=a.toLowerCase();
     return a,b;
}
uppercase("Hi Sandeep")
console.log(a);
console.log(b);


//   Write a function that takes a string as input and removes leading and trailing whitespace characters using the trim method.
console.log(" ");
console.log("4:");   
function trim(string) {
     return string.trim()

   }
 
   console.log(trim(" Sandeep "));

//    Write a function that takes a string as input and removes leading whitespace characters using the trimStart method.
console.log(" ");
console.log("5:");   
function trimStart(string) {
    return string.trimStart();
}
console.log("how are you");
console.log("  how are you");
console.log(trimStart("  how are you"));



//    Write a function that takes a string as input and removes trailing whitespace characters using the trimEnd method.

console.log(" ");
console.log("6:"); 
function trimEnd(str) {
    let a=str.trimEnd();
    console.log(a);
}
trimEnd("what are you doing  ")

//    Write a function that takes two strings as input and concatenates them using the concat method.
console.log(" ");
console.log("7:"); 
function concat(str1,str2){
  return str1.concat(str2)
 }
 console.log(concat("sandeep ", "reddy"));

 //    Write a function that takes a string and a substring as arguments and returns true if the substring is found in the string using the includes method.

 console.log(" ");
console.log("8:");

function includes(str1, str2)
{
   return str1.includes(str2)
}
console.log(includes("sandeep kumar reddy","ddy"));

//     Write a function that takes a string and a character as arguments and returns the index of the first occurrence of the character in the string using the indexOf method.

console.log(" ");
console.log("9:");
function index(str, ind) {
    return  str.indexOf(ind)
}
console.log(index("sandeep",5));          //   the  number is not  found in string so    it returns -1
console.log(index("sandeep"));           // default value is -1
console.log(index("sandeep","e"));        // 6 is out put


//     Write a function that takes a string and a character as arguments and returns the index of the last occurrence of the character in the string using the lastIndexOf method.

console.log(" ");
console.log("10:");
function lastIndex(str,ind){
   return  str.lastIndexOf(ind);
}
console.log(lastIndex("reddy","d"));

//     Write a function that takes a string, a start index, and an end index as arguments and returns the substring between the start and end indices using the slice method.

console.log(" ");
console.log("11:");
function slice(ind1, ind2, str) {
    return  str.slice(ind1,ind2)
}
console.log(slice(3,7,"hello world"));

//     Write a function that takes a string and a target substring as arguments and returns true if the string ends with the target substring using the endsWith method.
console.log(" ");
console.log("12:");
function endWith(str1, str2) {
    return str1.endsWith(str2)
}
console.log(endWith("sandeep","ep"));

//     Write a function that takes a string and a target length as arguments and pads the string with leading spaces until it reaches the target length using the padStart method.

console.log(" ");
console.log("13:");

function padStart(ind,str1,str2) {
    return str2.padStart(ind,str1)
}
console.log(padStart(30,"#","hello"));




//    Write a function that takes a string and a target length as arguments and pads the string with trailing spaces until it reaches the target length using the padEnd method.

console.log(" ");
console.log("14:");

function padEnd(ind,str1,str2) {
    return str2.padEnd(ind,str1)
}
console.log(padEnd(30,"#","hello"));



//      Write a function that takes a string and a number as arguments and returns a new string with the original string repeated the specified number of times using the repeat method.

console.log(" ");
console.log("15:");
function repeat(str,ind) {
    return str.repeat(ind) 
}
console.log(repeat("sandheep ",20));


//     Write a function that takes a string and a delimiter as arguments and splits the string into an array of substrings using the split method.

console.log(" ");
console.log("16:");
function split(str1,str2) {
    return str1.split(str2)
}
console.log(split("hello this is sandeep what can i help you"," "));



//     Write a function that takes a string, a start index, and an end index as arguments and returns the substring between the start and end indices using the substring method.
 
console.log(" ");
console.log("17:");
 function substring(str, start, end){
    return  str.substring(start,end)
 }
 console.log(substring("hello this is sandeep what can i help you",0,19));
 console.log(substring("hello this is sandeep what can i help you",10,19));
 console.log(substring("hello this is sandeep what can i help you",19,0));
 console.log(substring("hello this is sandeep what can i help you",1,0));


 //     Write a function that takes a string, a target substring, and a replacement string as arguments and replaces all occurrences of the target substring with the replacement string using the replace method.


 console.log(" ");
 console.log("18:");
 
 function replace(str1,str2,str3) {
    return str1.replaceAll(str2,str3)
 }
 console.log(replace(" i love you mounika reddy","love","❤️"));




 console.log(" ");
 console.log("18:");
 
 function replaceall(str1,str2,str3) {
    return str1.replaceAll(str2,str3)
 }
 console.log(replaceall(" i love you mounika reddy","o","❤️"));