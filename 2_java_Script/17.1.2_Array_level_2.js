
// Array Interview Programs (Level 2):

//     Implement a function to find the longest substring without repeating characters in a given string.
//     Find the kth largest element in an unsorted array.
//     Given an array of integers, find three numbers whose sum is closest to a given target.
//     Rotate a matrix (2D array) by 90 degrees clockwise in-place.
//     Implement a function to find the majority element (element that appears more than n/2 times) in an array.
//     Sort an array of 0s, 1s, and 2s (Dutch National Flag problem) without using extra space.
//     Given an array of intervals, merge overlapping intervals.
//     Find the shortest subarray with sum at least k in a non-negative array.
//     Determine if there exists a subarray with a sum equal to a given target in an unsorted array of non-negative numbers.
//     Given an array of integers, find the longest increasing subsequence.

