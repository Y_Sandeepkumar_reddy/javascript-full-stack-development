// Word Reversal:
//! Write a function that takes a sentence as input and reverses the order of words in the sentence while keeping the words themselves unchanged. For example, inputting "Hello world" should return "world Hello".

let str = "Hello World"; // output:  "World Hello"
let a = function stringReverse(str) {
  let a = str.split(" ").reverse();
  let b = a.join(" ");
  return b;
};
console.log(a(str));
// Anagram Detection:
//! Write a function that takes two strings as input and determines if they are anagrams of each other (i.e., contain the same letters in a different order). For example, "listen" and "silent" are anagrams.
let s1 = "listen";
let s2 = "silent";
let b = (s1, s2) => {
  let st1 = s1.toLowerCase().split("").sort().join("");
  let st2 = s2.toLowerCase().split("").sort().join("");

  return st1 === st2;
};
console.log(b(s1, s2));
// Palindrome Check:
//! Write a function that takes a string as input and determines if it's a palindrome (reads the same forwards and backwards). For example, "racecar" is a palindrome.
let s3 = "racecar";
function palindrom(str) {
  let b = "";
  for (key of str) {
    b = b + key;
  }
  if (str === b) {
    console.log(` The element is palindrom1 :${b}`);
  }
}
palindrom(s3);
//    or

function palindrom1(str) {
  let c = str.split("").reverse().join("");
  if (str === c) {
    console.log(`this is palindrom: ${c}`);
  }
}
palindrom1(s3);

// CamelCase to Snake Case:
//! Write a function that converts a string in CamelCase format (e.g., "helloWorld") to snake_case format (e.g., "hello_world").
function camelToSnakeCase(inputString) {
  return inputString.replace(
    /([A-Z])([a-z\d])/g,
    function (match, upper, lower) {
      upper = upper.toLowerCase();
      if (lower) {
        return upper + "_" + lower;
      } else {
        return upper;
      }
    }
  );
}

// Example usage
const camelCaseString = "helloWorld";
const snakeCaseString = camelToSnakeCase(camelCaseString);
console.log(snakeCaseString); // Output: hello_world

// String Compression:
//! Write a function that takes a string with repeated characters and compresses it by replacing consecutive repeating characters with a single character followed by the number of repetitions. For example, "aabcccccaaa" should be compressed to "a2b1c5a3".



// Caesar Cipher:
// Implement a Caesar cipher encryption function that takes a string and a shift value as input and returns the encrypted string. The Caesar cipher shifts each letter of the alphabet by the specified value.

// String Rotation:
// Write a function that takes two strings as input and determines if one string is a rotation of the other string. For example, "waterbottle" is a rotation of "erbottlewat".

// Longest Substring Without Repeating Characters:
// Write a function that takes a string as input and returns the length of the longest substring without repeating characters. For example, the longest substring without repeating characters in "abcabcbb" is "abc", which has a length of 3.

// Sentence Capitalization:
// Write a function that takes a sentence as input and capitalizes the first letter of each word in the sentence while converting the rest of the letters to lowercase. For example, "hello world" should become "Hello World".

// String Permutations:
// Write a function that generates all permutations of a given string. For example, the permutations of "abc" are "abc", "acb", "bac", "bca", "cab", and "cba"
