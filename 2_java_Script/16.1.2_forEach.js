//  forEach 


/*
--> forEach also is used to iterate each element ina an  array or object
-->   forEach method never return anny thing it return undefined so  if we try to return result of forEach it return undefined .

-->   we can iterate array using forEach but we can not use in object

*/
//?  Usage: forEach is used to perform an action on each element of an array.

//?      -->  Callback Function: It takes a callback function as an argument to process each array element.
//?      -->  Iterating Elements: It goes through array elements in order from index 0 to the last index.
//?      -->  Returning Values: forEach doesn't create a new array; it simply applies the callback function to each element.
//?      -->  Modifying Array: You can modify array elements inside the callback function.
//?      -->  Breaking Loop: Unlike for loops, you can't break out of a forEach loop prematurely.




const { colors, fruits, numbers, data } = require("./0_data");

console.log("1:");

let a=(n)=>{
    let final=[]
  n.forEach((element) => {
      final.push(element)                                 // this operation for arrays
  });
  return final
}                                           
console.log(a(colors))






// let b = (obj) => {
//   let object = {};
//   obj.forEach((element, index) => {
//     object[index] = element;                    // can can not use for object
//   });
//   return object;
// };

// console.log(b(data));

console.log("--------------------------------------------------------------------------------------------------------------------------------------------");
console.log("2:");

let c = (obj) => {
    let newObj = {};
    Object.keys(obj).forEach((key) => {
      newObj[key] = obj[key];
    });
    return newObj;
  };
  
  console.log(c(data));

console.log("--------------------------------------------------------------------------------------------------------------------------------------------");
console.log("3:");

  let d = (obj) => {
    let newObj = {};
    Object.keys(obj).forEach((key) => {
    //   newObj[key] = obj[key];
    if (key==='employees') {
         newObj[key] = obj[key];

    }
    });
    return newObj;
  };
  
  console.log(d(data));
  

  console.log("--------------------------------------------------------------------------------------------------------------------------------------------");
console.log("4:");

  let z=((data)=>{
    let desc={}
    for (const key in data) {
           if(key==='employees')
             {
                data[key].forEach(element => {
                    element.projects.forEach(p => {
                        if(p.name==="Project A")
                        {
                            desc[p.name] = p.description;

                        }
                    });
                });
             }
        }
        return desc
    }
)
console.log(z(data));












