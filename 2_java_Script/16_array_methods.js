// Array Methods

/*   --> Some of the array methods to perform operations,
        These methods allow you to manipulate, iterate over, or perform actions on the elements of an array. Some common array methods in JavaScript include:

1. flat         :    2.concat         :    3.push         :    4.pop         :       
5.shift         :    6.unshift        :    7.indexOf      :    8.lastIndexOf :
9.includes      :   10.reverse        :    11.split       :   12.slice       :


for unshift we ahave to return at last
*/

let colors = [
    'Red', 'Green', 'Blue', 'Yellow', 'Orange',
    'Purple', 'Pink', 'Brown', 'Black', 'White',
    'Gray', 'Cyan', 'Magenta', 'Lime', 'Teal',
    'Navy', 'Maroon', 'Olive', 'Silver', 'Gold','Black'
];
let fruits = [
    'Apple', 'Banana', 'Orange', 'Grapes', 'Watermelon',
    'Pineapple', 'Mango', 'Strawberry', 'Cherry', 'Kiwi',
    'Peach', 'Plum', 'Pear', 'Lemon', 'Coconut',
    'Apricot', 'Blueberry', 'Raspberry', 'Cantaloupe', 'Pomegranate'
];
let  numbers=[1,2,3,[4,5,[6,7,8,[9,10,11,12]]]];


//Flat
console.log(" ");                  // immutable   cannoot change original array
console.log("1:");
let a=(numb,n)=>{
    return  numb.flat(n)
}
console.log(a(numbers,2))   //  [1,2,3,4,5,6,7,8,[9,10,11,12]]
console.log(a(numbers,3))   //  [1,2,3,4,5,6,7,8,9,10,11,12]
console.log(a(numbers,1))   //[1,2,3,4,5[6,7,8,[9,10,11,12]]]

// -->  The flat method in JavaScript is used to flatten an array by one level. It returns a new array with any sub-arrays flattened into it.




// push
console.log("------------------------------------------------------------------------------------------------------ ");
console.log("2: ");

                                 // mutable can change the original array
let n2=[1,2,3,4,5]
function push(n){
    console.log(n2);
  let z= n2.push(n)
   console.log(`${z}  is  new length of array `)
   console.log(n2);
}
push(7)




// pop
console.log("------------------------------------------------------------------------------------------------------ ");
console.log("3: ");

function pop(p) {
    console.log(numbers);                           // mutable can change the original array
    console.log(" ");
    return p.pop()
}

console.log(pop(numbers));
console.log(numbers);
//  by using pop we can delete  one element at a time



// Shift 
console.log("------------------------------------------------------------------------------------------------------ ");
console.log("4: ");
//  shift is used to remove element in frist index and moves the remaining to front


let result=(fruits)=>{
    console.log(fruits);
  return fruits.shift()                                 // mutable can change the original array
}
console.log(result(fruits)+" is removed from list");
console.log(fruits);



// unshift
console.log("------------------------------------------------------------------------------------------------------ ");
console.log("5: ");
// it is used to add elements in array frist index

let n5=(c,n)=>{
  c.unshift(n)                                            // mutable can change the original array
 return c;
}
console.log(n5(colors,"boomboom"));





//  includes
console.log("------------------------------------------------------------------------------------------------------ ");
console.log("6: ");
   // it ruturns the true or false based on the  value is  included or not in the array

function include(str,ch){
 return  str.includes(ch)
}

console.log(include(fruits,"mango"));    // false       case-sensitive
console.log(include(fruits,"Mango"));    // true   





//concat
console.log("------------------------------------------------------------------------------------------------------ ");
console.log("7: ");

function concat(str1,str2) {
     return str1+str2
}
console.log(concat(fruits,colors));


//indexOf
console.log("------------------------------------------------------------------------------------------------------ ");
console.log("8: ");
 function index(colors,str) {
     return  colors.indexOf(str)
 }
 console.log(index(colors,"black"));    // -1      case-sensitive
 console.log(index(colors,"Black"));    // 9



 //latIndexOf
console.log("------------------------------------------------------------------------------------------------------ ");
console.log("9: ");
 function lastindex(colors,str) {
     return  colors.lastIndexOf(str)
 }
 console.log(lastindex(colors,"black"));    // -1      case-sensitive
 console.log(lastindex(colors,"Black"));    // 21 



 //reverse 
 console.log("------------------------------------------------------------------------------------------------------ ");
console.log("10: ");

 let results=(fruits)=>{
     return fruits.reverse()
 }
 console.log(fruits);
 console.log(results(fruits));


 //splice
 console.log("------------------------------------------------------------------------------------------------------ ");
 console.log("11: ");

 let n11=(c,n)=>{
    return c.splice(n)                // splice is  used to delete perticular element in an array
 }
 console.log(colors);
 console.log(n11(colors,1));
 console.log(n11(colors,6));




 //slice
 console.log("------------------------------------------------------------------------------------------------------ ");
 console.log("12: ");


fru()
function fru() {
    let n12=(str,n1,n2)=>{
        return str.slice(n1,n2)
       }
       console.log(fruits);
       console.log(n12(fruits,2,3));                      // it print from  index 2 to index 3    there is only one so only one  element prints
}