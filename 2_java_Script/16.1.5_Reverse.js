//?  The reverse() method reverses the order of the elements in an array.

//?  The reverse() method overwrites the original array.



// array

let a=[3,5,7,9,10,1,34,79]

a.sort((a,b)=>a-b)
a.reverse()
console.log(a);



let b=[3,5,7,9,10,1,34,79]
const n=b.concat().reverse()
console.log(b);
console.log(n);

console.log(a.concat(b))




// object

let str=" coding is fun"


let d=(str)=>{
    return str.split("").reverse().join("")
}
console.log(d(str));






// Input array
const arr = [1, 2, 3, 4, 5];

// Call the reverseArrayElements function
const reversedArr = reverseArrayElements(arr);
reversedArr.reverse()
function reverseArrayElements(arr) {
     return arr.sort((a,b)=>a-b)
}

// Output the reversed array
console.log('Reversed Array:', reversedArr);

// output  Reversed Array: [5, 4, 3, 2, 1]



/*


    Reverse an Array:
    Write a function that takes an array as input and returns a new array with the elements reversed.

    Example:
    Input: [1, 2, 3, 4, 5]
    Output: [5, 4, 3, 2, 1]

    Reverse Every Word in a Sentence:
    Write a function that takes a sentence as input and returns a new sentence with each word reversed.

    Example:
    Input: "Hello world how are you"
    Output: "olleH dlrow woh era uoy"

    Reverse a Linked List:
    Write a function to reverse a singly linked list. Each node in the linked list has a value and a pointer to the next node.

    Example:
    Input: 1 -> 2 -> 3 -> 4 -> 5 (linked list)
    Output: 5 -> 4 -> 3 -> 2 -> 1 (reversed linked list)

    Reverse Digits of a Number:
    Write a function that takes a number as input and returns the number with its digits reversed.

    Example:
    Input: 12345
    Output: 54321

*/


/*

    Reverse a String:
    Write a function that takes a string as input and returns the reversed version of the string.

    Example:
    Input: "hello"
    Output: "olleh"
*/
function reverseString(str)
{
    let a=str.split("")
    a.reverse()
    let b=a.join("")
    console.log(b);
}
reverseString("hello")

// Reverse an Array:
// Write a function that takes an array as input and returns a new array with the elements reversed.

let e="Write a function that takes an array as input and returns a new array with the elements reversed" ;

function reverseArray(str) {
    let a=str.split(" ")
    a.reverse()
    let b=a.join("")
    b.split("")
    console.log(b);
}
console.log(reverseArray(e));



let st="Write a function that takes an array as input and returns a new array with the elements reversed";


let z=(str)=>{
    let z=[]
  let a=str.split(" ")
    for(let i=0;i<a.length;i++)
    {
    let b= a[i].split("")
    b.reverse()
    let c=b.join("")
    z.push(c)
    }
    console.log(z.length)
    return z.join(" ")
  
  
    
}
console.log(z(st))