/*
-->//?     The every() method executes a function for each array element.
-->//?     The every() method returns true if the function returns true for all elements.
-->//?     The every() method returns false if the function returns false for one element.
--> //?    The every() method does not execute the function for empty elements.
-->//?     The every() method does not change the original array

Return Value
Type 	   Description
Boolean	   true if all elements pass the test, otherwise false.

*/

const array = [1, 2, 3, 4, 5, 6, 7, 8, 9];

let a = array.every(every_method);
function every_method(params) {
  return params > 0;
}
console.log(a);

let z = array.every((array) => {
  return array < 0;
});
console.log(z);

let persons = [
  {
    names: "Reddy",
  },
  {
    names: "Reddy",
  },
  {
    names: "Reddy",
  },
  {
    names: "Reddy",
  },
];

let za = persons.every((per) => {
  return per.names == "Reddy";
});
console.log(za);

let numbers = [
  [1, 2, 3],
  [4, 5, 6],
  [7, 8, 9],
];

let zb = numbers.every((arr) => Array.isArray(arr));
console.log(zb);

// Example 1: Check if all numbers in an array are positive
let numbers = [1, 2, 3, 4, 5];
let allPositive = numbers.every((num) => num > 0);

if (allPositive) {
  console.log("All numbers are positive");
} else {
  console.log("Some numbers are not positive");
}

// Example 2: Check if all strings in an array have length greater than 3
let words = ["apple", "banana", "cherry", "orange"];
let allLongWords = words.every((word) => word.length > 3);

if (allLongWords) {
  console.log("All words have length greater than 3");
} else {
  console.log("Some words do not have length greater than 3");
}

// Example 3: Check if all elements in an array are even numbers
let nums = [2, 4, 6, 8, 10];
let allEven = nums.every((num) => num % 2 === 0);

if (allEven) {
  console.log("All numbers are even");
} else {
  console.log("Some numbers are not even");
}

// Example 4: Check if all elements in an array are arrays themselves
let arrOfArrays = [
  [1, 2],
  [3, 4],
  [5, 6],
];
let allArrays = arrOfArrays.every((arr) => Array.isArray(arr));

if (allArrays) {
  console.log("All elements are arrays");
} else {
  console.log("Some elements are not arrays");
}
