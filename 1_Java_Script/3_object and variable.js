//object 

//   An object is a Non-proimitive type of value , it contain state and behaviour,
//   object contain the collection ov values
//   annything writen in a curly brackets is know as onject in javascript. 
//   array also one type of object.

let a= typeof{age:24}
console.log(a);



// variable  
/*
 -> variable is a container where we can store a values.
 ->in javascript ther are 3 types of  to create variables
 ->   let     var     const
  
    let and const have block scope.
    let and const can not be redeclared.
    let and const must be declared before use.
    let and const does not bind to this.
    let and const are not hoisted.
     
    var is function scope 
    var is  hoisted to the top of their containing function or global scope.his means you can use a variable declared with var before it is actually declared in the code.
    var can be re-assigned through the function. 
    

*/
// var example
console.log(myVar); // Output: undefined (no error due to hoisting)
var myVar = 'Hello';

// let example
console.log(myLet); // Output: ReferenceError: Cannot access 'myLet' before initialization
let myLet = 'World';

// const example
const PI = 3.14;
PI = 3.14159; // Error: Assignment to constant variable

const person = { name: 'John' };
person.age = 30; // Allowed
console.log(person); // Output: { name: 'John', age: 30 }
