// Function

// -> A function is a set of statements which is used to perform perticular task.      OR

// -> A function is a block of code , designed to perform  a perticular task  and  Java script  function is executed when  something invoked it(Calls it).

function twosum(a,b)
{
console.log(`the sum of ${a,b}is: `+(a+b))
 
}
twosum(2,9)


// function  -   is function, is also called  key word
// twosum    - is the name of the function
//   a, b    - is the perameters




//  function with single inpuit
log(6);
function log(n)
{
     let fact=1
    for(let a=1;a<=n;a++){
        fact*=a
    }
    console.log(`the factorial of ${n} is: `+fact)
}

// function with multiple inputs

multi_input("sandeep","Software developer");
multi_input("sandheep","Software developer anf formor");
multi_input("reddy"," gentlemen");
multi_input("suresh"," Software developer and author");
multi_input("govardhan","Engineer");

function multi_input(name,occupation)
{
   console.log(`hi this is ${name} and  i am a ${occupation}`)   
}



// TYPES- Named function , Annomous function , Arrow function

// 1. function decleration 
// 2. function expression
// 3. Annomous function
// 4. Arrow function
// 5.Arguments


// 1 . function decleration

console.log(" ");
console.log(" ");

console.log("function decleration");

 function sum(a,b)
 {
    console.log(a+b)
 }
 sum(5,7)



//---------------------------------------------------------------------------
 console.log(" ");
 console.log(" ");
 // 2. Function expression


 let tsum=function sum(a,b)
 {
    return a+b;
 }
 console.log(tsum(9,99))    //    this is called function expression   ex: let  a= 20;  anything after = is called expression
 


//-------------------------------------------------------------------------
 console.log(" ");
console.log(" ");
 // Annomous function

 const z=function(a,b)
 {
    return a*b+a+b-b*a;
 }
 console.log(z(9,4));

 //---------------------------------------------------------------
console.log(" ");
console.log(" ");

const y=(a,b,c,d)=>{
  return a+b+c+d+a*b*c-d/a;
}
console.log(y(4,6,8,7));
//----------------------------------------------------------------------------------------------------------------------------
//IIFE -immediate invoked function

console.log(" ");
console.log(" ");





(function()
{


})();


(function(name)
{
console.log(`this is IIFE : ${name}`);
})("sandheep");

/*
--> when we are creatinfg a function a function we are adding that to the global function or object
-->if two persons are working  with you on this project and he is also using same function name that will also added to the global object , that will replace  our already existing .
--> this is called poluting the global object.

*/

console.log(" ");


(function () {
   function sum(a,b) {
    console.log(a+a*b+b)
   } 
   sum(23,4)
})();