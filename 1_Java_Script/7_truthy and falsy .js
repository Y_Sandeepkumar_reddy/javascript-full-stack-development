// truthy7 and falsy values 

// when a value is converted into boolean the the result is "true" is called truthy value and when  the result is false is called  falsy values.

    let a=Boolean(29);
   let b= Boolean("hi");
   let c= Boolean(Infinity);

   let d=Boolean(NaN);
   let e=Boolean(undefined);
   let f= Boolean(0);
   let g=Boolean("")
   let h=Boolean(false)

   console.log(a)
   console.log(b)

   console.log(c)
   console.log(d)

   console.log(e)
   console.log(f)

   console.log(g)
   console.log(h)

// there are five falsy values Nan, undefined ,0 ,"" ,false . othere than this are truthly values 
