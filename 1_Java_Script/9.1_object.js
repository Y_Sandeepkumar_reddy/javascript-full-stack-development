//   differenbt operation on object

// .  ,  []     dot(.) and  [] square bracket is used to access values in object

let user={
    Name:"sandheep",
    age:24,
    sex:"male",
    BirthPlace:"kadap",
    isAdult:true
}
   

                             //1. accessing values
console.log(user.age)
console.log(user["isAdult"])
console.log(user["Name"])             // when using the []  for accessing the value    must encode with  ["key"]




                              // 2. add new property

user["girlfriend"]="yes or no"
console.log(user)

user.status="single"
console.log(user)


                              // 3. update a  value

                        
user.age=23;
user["girlfriend"]="no"
console.log(user)




                                // Deleting a value or property

delete user.status;
console.log(user);

delete user["girlfriend","isAdult"]
console.log(user);

/*
rules:

1. if we have multiple words in key name have to put in double quotes
2. when we are acc3essing multiple word or numbers  dot (.) operetor is not work
3. if we  accessing not avalable values it shows   undefined.

*/