let data = [
    {
        id: 1,
        name: 'John Doe',
        details: {
            age: 25,
            profession: 'Software Engineer',
            address: '123 Main St, City',
            contact: {
                email: 'john.doe@example.com',
                phone: '123-456-7890'
            }
        },
        hobbies: ['Reading', 'Swimming', 'Coding', 'Gaming', 'Cooking']
    },
    {
        id: 2,
        name: 'Jane Smith',
        details: {
            age: 30,
            profession: 'Artist',
            address: '456 Elm St, Town',
            contact: {
                email: 'jane.smith@example.com',
                phone: '987-654-3210'
            }
        },
        hobbies: ['Painting', 'Dancing', 'Traveling', 'Hiking', 'Photography']
    },
    // Add more objects with increased inner object size here...
    {
        id: 3,
        name: 'Liam Taylor',
        details: {
            age: 27,
            profession: 'Teacher',
            address: '789 Oak St, Village',
            contact: {
                email: 'liam.taylor@example.com',
                phone: '456-789-0123'
            }
        },
        hobbies: ['Traveling', 'Mountain Biking', 'Photography', 'Reading', 'Writing']
    },
    {
        id: 4,
        name: 'Ava Anderson',
        details: {
            age: 22,
            profession: 'Writer',
            address: '555 Pine St, County',
            contact: {
                email: 'ava.anderson@example.com',
                phone: '789-012-3456'
            }
        },
        hobbies: ['Hiking', 'Painting', 'Writing Poetry', 'Gardening', 'Singing']
    }
];

// console.log(data);

data.forEach(element => {
  console.log(element.details.contact.phone)   
});


data.forEach(element => {
     let a=(element.details.age)
    //  console.log(typeof(a))
     if(a<27)
     {
        console.log(element.name+`: age is below ${a}`)
     }
});