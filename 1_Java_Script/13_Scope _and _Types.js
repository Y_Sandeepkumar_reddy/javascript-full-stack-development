/* SCOPE   -  Scope is nothing but visibulity and accessabulity of variable in a program
 Types -Global scope ,Local scope,block scope,function scope
 1. Global Scope: Variables declared outside of any function or block have global scope and can be accessed from anywhere in the program.
 2.Local Scope: Variables declared inside a function or block have local scope and can only be accessed within that specific function or block.
 3.Function Scope:  Variables declared using var inside a function have function scope and are visible throughout the entire function.
 4. Block Scope: Variables declared using let or const inside a block (e.g., within curly braces {}) have block scope and are only accessible within that block.

 */
let user="arya";           // global  scope
{
    let nam="sandeep"     // local scope
}
function names()
{
    let lastName="reddy"    // function scope
}
names()
 
console.log(user);          //arya
console.log(nam);           // error, user nam not defined
console.log(lastName);      //  error, not defined



  // benifits of scope  -   Security , Naming Collesion