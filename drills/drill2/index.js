// Download the contents of "Harry Potter and the Goblet of fire" using the command line from here
   wget https://raw.githubusercontent.com/bobdeng/owlreader/master/ERead/assets/books/Harry%20Potter%20and%20the%20Goblet%20of%20Fire.txt

// Print the first three lines in the book
  head -n3 'Harry Potter and the Goblet of Fire.txt'


//Print the last 10 lines in the book
tail -n10 'Harry Potter and the Goblet of Fire.txt'

// How many times do the following words occur in the book?

//     Harry
//     Ron
//     Hermione
//     Dumbledore
grep -o -i 'Harry' 'Harry Potter and the Goblet of Fire.txt' |w -l
grep -o -i 'Ron' 'Harry Potter and the Goblet of Fire.txt' | wc -l
grep -o -i 'Hermione' 'Harry Potter and the Goblet of Fire.txt' |w -l
grep -o -i 'Dumbledore' 'Harry Potter and the Goblet of Fire.txt' | wc -l

// Print lines from 100 through 200 in the book
sed -n '100,200p' 'Harry Potter and the Goblet of Fire.txt'

//How many unique words are present in the book?
